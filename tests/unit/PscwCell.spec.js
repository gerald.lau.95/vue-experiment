import { expect } from 'chai';
import sinon from 'sinon';
import Element from 'element-ui';
import { mount, createLocalVue } from '@vue/test-utils';
import { addEventLogger } from './log-events';

import PscwCell from '../../src/components/PscwCell.vue';

const localVue = createLocalVue();

localVue.use(Element);

// To test child component without actual render it
const elTableStub = localVue.component('el-table-stub', {
  name: 'el-table-stub',
  render(h) {
    return h('div', { class: 'el-table-stub' }, [this.$slots.default]);
  },
  props: {
    data: Array,
    border: Boolean,
    maxHeight: String,
    rowKey: String,
    fit: Boolean,
  },
});
const elTableColumnStub = localVue.component('el-table-column-stub', {
  name: 'el-table-column-stub',
  render(h) {
    return h('div', {
      class: {
        'el-table-column-stub': true,
        [this.className]: true,
      },
      on: {
        click: () => {
          this.$emit('clicked');
        },
      },
    }, [
      this.$scopedSlots.header && this.$scopedSlots.header(),
      this.$scopedSlots.default && this.$scopedSlots.default({ row: {} }),
    ]);
  },
  props: {
    className: String,
    sortable: Boolean,
    fixed: Boolean,
    prop: String,
    label: String,
    minWidth: String,
    align: String,
    headerAlign: String,
  },
});
addEventLogger(elTableStub);
addEventLogger(elTableColumnStub);

/**
 * Test data
 */
const defaultHeader = [
  {
    label: 'ID',
    key: 'id',
    width: '50',
    fixed: true,
    sortable: true,
    align: 'center',
  },
  {
    label: 'Application',
    key: 'name',
    width: '180',
    fixed: true,
    sortable: true,
  },
];
const table = [
  {
    id: 1,
    name: 'Top 10 Travel Website',
    api_key: '12345678-1234-1234-1234-123456781234',
    webhook: '',
    created_at: '2015-07-14T14:06:02.64277Z',
    updated_at: '2016-12-13T04:20:24.748573Z',
    54: true,
  },
];
const tableHeader = [
  {
    label: 'agoda::agoda',
    tag: 'Zumata',
    id: 54,
    key: '54',
    zps_code: 'agoda::agoda',
    isPartnerActive: true,
    isSupplierActive: true,
  },
  {
    label: 'agoda::agoda',
    tag: 'Mock',
    id: 55,
    key: '55',
    zps_code: 'agoda::agoda',
    isPartnerActive: true,
    isSupplierActive: false,
  },
  {
    label: 'ean::ean',
    tag: 'Mock',
    id: 56,
    key: '56',
    zps_code: 'ean::ean',
    isPartnerActive: false,
    isSupplierActive: false,
  },
];

const sortChangedHandler = sinon.stub();
const supplierToggledStub = sinon.stub();

const factory = propsData => mount(PscwCell,
  {
    context: {
      props: {
        table,
        th: tableHeader,
        defaultTh: defaultHeader,
        ...propsData,
      },
    },
    stubs: {
      'el-table': elTableStub,
      'el-table-column': elTableColumnStub,
    },
    listeners: {
      sortChange: sortChangedHandler,
      supplierToggled: supplierToggledStub,
    },
    localVue,
  });

describe('PscwCell.vue', () => {
  it('should pass props.table to elTable.props.data', () => {
    const wrapper = factory();
    const tableStub = wrapper.find('.el-table-stub');
    expect(tableStub.props('data')).to.equal(table);
  });

  it('should generate 2 elTableColumn from props.defaultTh and their order is following the props.defaultTh array sequence.', () => {
    const wrapper = factory();

    // We are using label to test the order
    const col1 = wrapper.findAll(elTableColumnStub).at(0);
    expect(col1.props('label')).to.equal(defaultHeader[0].label);

    const col2 = wrapper.findAll(elTableColumnStub).at(1);
    expect(col2.props('label')).to.equal(defaultHeader[1].label);
  });

  it('should proxy props.defaultTh[{width}] to first 2 columns props.minWidth respectively', () => {
    const wrapper = factory();

    const col1 = wrapper.findAll(elTableColumnStub).at(0);
    expect(col1.props('minWidth')).to.equal(defaultHeader[0].width);

    const col2 = wrapper.findAll(elTableColumnStub).at(1);
    expect(col2.props('minWidth')).to.equal(defaultHeader[1].width);
  });

  it('should proxy props.defaultTh[{fixed}] to first 2 columns props.fixed respectively', () => {
    const wrapper = factory();

    const col1 = wrapper.findAll(elTableColumnStub).at(0);
    expect(col1.props('fixed')).to.equal(defaultHeader[0].fixed);

    const col2 = wrapper.findAll(elTableColumnStub).at(1);
    expect(col2.props('fixed')).to.equal(defaultHeader[1].fixed);
  });

  it('should proxy props.defaultTh[{align}] to first 2 columns props.align respectively', () => {
    const wrapper = factory();

    const col1 = wrapper.findAll(elTableColumnStub).at(0);
    expect(col1.props('align')).to.equal(defaultHeader[0].align);

    const col2 = wrapper.findAll(elTableColumnStub).at(1);
    expect(col2.props('align')).to.equal(defaultHeader[1].align);
  });

  it('should proxy props.defaultTh[{sortable}] to first 2 columns props.sortable respectively', () => {
    const wrapper = factory();

    const col1 = wrapper.findAll(elTableColumnStub).at(0);
    expect(col1.props('sortable')).to.equal(defaultHeader[0].sortable);

    const col2 = wrapper.findAll(elTableColumnStub).at(1);
    expect(col2.props('sortable')).to.equal(defaultHeader[1].sortable);
  });

  it('should generate the rest columns from props.th as credentail column', () => {
    const wrapper = factory();

    const col = wrapper.findAll(elTableColumnStub).at(2);
    expect(col.html()).to.contain('agoda::agoda');
  });

  describe('generated credential column', () => {
    it('should group the props.th by "label" property', () => {
      const wrapper = factory();
      const col = wrapper.findAll('.el-table-column-stub.th__supplier').at(0);
      expect(col.exists()).to.be.true;
      const col2 = wrapper.findAll('.el-table-column-stub.th__supplier').at(1);
      expect(col2.exists()).to.be.true;
      expect(col.find('.rotate-60').text()).to.not.equal(col2.find('.rotate-60').text());
    });

    it('should has class "th__supplier--disabled" when partner is inactive', () => {
      const wrapper = factory({
        th: [
          {
            label: 'agoda::agoda',
            tag: 'Zumata',
            id: 54,
            key: '54',
            zps_code: 'agoda::agoda',
            isPartnerActive: false,
            isSupplierActive: true,
          },
        ],
      });
      const col = wrapper.findAll(elTableColumnStub).at(2);
      expect(col.classes()).to.contain('th__supplier--disabled');
    });

    it('should has class "th__supplier--disabled" when supplier is inactive', () => {
      const wrapper = factory({
        th: [
          {
            label: 'agoda::agoda',
            tag: 'Zumata',
            id: 54,
            key: '54',
            zps_code: 'agoda::agoda',
            isPartnerActive: true,
            isSupplierActive: false,
          },
        ],
      });
      const col = wrapper.findAll(elTableColumnStub).at(2);
      expect(col.classes()).to.contain('th__supplier--disabled');
    });

    it('should has class "th__supplier--disabled" when both supplier and partner are inactive', () => {
      const wrapper = factory({
        th: [
          {
            label: 'agoda::agoda',
            tag: 'Zumata',
            id: 54,
            key: '54',
            zps_code: 'agoda::agoda',
            isPartnerActive: true,
            isSupplierActive: false,
          },
        ],
      });
      const col = wrapper.findAll(elTableColumnStub).at(2);
      expect(col.classes()).to.contain('th__supplier--disabled');
    });

    it('should has a global checkbox', () => {
      const wrapper = factory();
      const col = wrapper.findAll(elTableColumnStub).at(2);
      expect(col.contains('input.th__supplier__checkbox[type="checkbox"]')).to.equal(true);
    });

    it('the global checkbox should be checked when both supplier and partner are active', () => {
      const wrapper = factory();
      const col = wrapper.findAll(elTableColumnStub).at(2);
      const checkbox = col.find('input.th__supplier__checkbox[type="checkbox"]');
      expect(checkbox.element.checked).to.equal(true);
    });

    it('the global checkbox should be unchecked when supplier is inactive', () => {
      const wrapper = factory({
        th: [
          {
            label: 'agoda::agoda',
            tag: 'Zumata',
            id: 54,
            key: '54',
            zps_code: 'agoda::agoda',
            isPartnerActive: true,
            isSupplierActive: false,
          },
        ],
      });
      const col = wrapper.findAll(elTableColumnStub).at(2);
      const checkbox = col.find('input.th__supplier__checkbox[type="checkbox"]');
      expect(checkbox.element.checked).to.equal(false);
    });

    it('the global checkbox should be unchecked when partner is inactive', () => {
      const wrapper = factory({
        th: [
          {
            label: 'agoda::agoda',
            tag: 'Zumata',
            id: 54,
            key: '54',
            zps_code: 'agoda::agoda',
            isPartnerActive: false,
            isSupplierActive: true,
          },
        ],
      });
      const col = wrapper.findAll(elTableColumnStub).at(2);
      const checkbox = col.find('input.th__supplier__checkbox[type="checkbox"]');
      expect(checkbox.element.checked).to.equal(false);
    });

    it('When user click the global checkbox el-table-column should emit "supplierToggled" event', () => {
      supplierToggledStub.reset();
      const wrapper = factory();
      const col = wrapper.findAll(elTableColumnStub).at(2);
      const checkbox = col.find('input.th__supplier__checkbox[type="checkbox"]');
      checkbox.trigger('click');
      expect(supplierToggledStub.calledOnce).to.equal(true);
    });
    it('the "supplierToggled" event should be emited with { value, zpsCode, sId }', () => {
      supplierToggledStub.reset();
      const wrapper = factory();
      const col = wrapper.findAll(elTableColumnStub).at(2);
      const checkbox = col.find('input.th__supplier__checkbox[type="checkbox"]');
      checkbox.trigger('click');
      const { value, zpsCode, sId } = supplierToggledStub.args[0][0];
      expect(value).to.false;
      expect(zpsCode).to.equal('agoda::agoda');
      expect(sId).to.equal(54);
    });
    it('When user click the global checkbox in el-table-column the click event should be captured and stopPropagating', () => {
      supplierToggledStub.reset();
      const wrapper = factory();
      const col = wrapper.findAll(elTableColumnStub).at(2);
      const checkbox = col.find('input.th__supplier__checkbox[type="checkbox"]');
      checkbox.trigger('click');
      expect(col.emitted()).to.be.empty;
    });
  });
});
