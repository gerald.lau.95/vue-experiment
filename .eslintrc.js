module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    'no-underscore-dangle': 'off',
    'import/prefer-default-export': 'off',
    'object-curly-newline': ['error', {
      ObjectExpression: { minProperties: 5, multiline: true, consistent: true },
      ObjectPattern: { minProperties: 5, multiline: true, consistent: true },
      ImportDeclaration: { minProperties: 5, multiline: true, consistent: true },
      ExportDeclaration: { minProperties: 5, multiline: true, consistent: true },
    }],
    'no-param-reassign': ['error', { 'props': true, 'ignorePropertyModificationsFor': ['el', 'state', 'pV'] }],
  },
  settings: {
    // This is to fix my eslint extension at VsCode
    // 'import/resolver': 'node',
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
