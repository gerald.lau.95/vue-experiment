module.exports = {
  css: {
    loaderOptions: {
      sass: {
        includePaths: ['./node_modules'],
        data: '@import "element-ui/packages/theme-chalk/src/common/var.scss";',
      },
    },
  },
  devServer: {
    // This is very handy on cross-origin problem
    // proxy: `http://${process.env.LOCAL_DEV_IP}:4000`
  },
};
