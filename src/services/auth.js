import ky from 'ky';
import Svcs from './Services';

export const auth = async (email, password) => ky.post('/auth', { email, password });

export const permissions = async () => Svcs.get('/permissions').json();
