import ky from 'ky';
import store from '../stores';

/**
 * @param authToken <string> - return from
 * @param isAdminHeaderIncluded <boolean>
 */
export default ky.extend({
  hooks: {
    beforeRequest: [
      (options) => {
        const authToken = store.state.me.claims.token;
        // const clientId = store.state.me.claims.client_id;
        options.headers.append('z-auth-token', authToken);
        options.headers.append('z-include-admin-header', !!authToken);
        // options.headers.append('z-client-product-ID', 667);
      },
    ],
  },
});
