import Svcs from './Services';

export const getPartners = async ({ cp }) => {
  try {
    return Svcs('/admin/partner/list', {
      method: 'GET',
      hooks: {
        beforeRequest: [
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    return {};
  }
};

export const getSuppliers = async ({ cp }) => {
  try {
    return Svcs('/admin/supplier/list', {
      method: 'GET',
      hooks: {
        beforeRequest: [
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    return {};
  }
};

export const getCredentails = async ({ cp }) => {
  try {
    return Svcs('/admin/credential/list', {
      method: 'GET',
      hooks: {
        beforeRequest: [
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    return {};
  }
};

export const getWhitelists = async ({ cp }) => {
  try {
    return Svcs('/admin/whitelist_credential/list', {
      method: 'GET',
      hooks: {
        beforeRequest: [
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    return {};
  }
};

export const getApplications = async ({ cp }) => {
  try {
    return Svcs('/admin/application/list', {
      method: 'GET',
      hooks: {
        beforeRequest: [
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    return {};
  }
};

export const postEnableSupplier = async ({ sId, cp }) => {
  try {
    return Svcs(`/admin/supplier/${sId}/enable`, {
      method: 'POST',
      hooks: {
        beforeRequest: [
          // TODO: Why this is needed?
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const postDisableSupplier = async ({ sId, cp }) => {
  try {
    return Svcs(`/admin/supplier/${sId}/disable`, {
      method: 'POST',
      hooks: {
        beforeRequest: [
          // TODO: Why this is needed?
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const postApplicationCredentialWhitelist = async ({ appId, cId, cp }) => {
  try {
    return Svcs(`/admin/application/${appId}/whitelist_credential/${cId}`, {
      method: 'POST',
      hooks: {
        beforeRequest: [
          // TODO: Why this is needed?
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const deleteApplicationCredentialWhitelist = async ({ appId, cId, cp }) => {
  try {
    return Svcs(`/admin/application/${appId}/whitelist_credential/${cId}`, {
      method: 'DELETE',
      hooks: {
        beforeRequest: [
          // TODO: Why this is needed?
          options => options.headers.append('z-client-product-ID', cp),
        ],
      },
    }).json();
  } catch (err) {
    console.error(err);
    throw err;
  }
};
