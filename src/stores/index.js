import Vue from 'vue';
import Vuex from 'vuex';

import me from './modules/me';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  strict: debug,
  modules: {
    me,
  },
  state: {

  },
  mutations: {

  },
  actions: {

  },
});
