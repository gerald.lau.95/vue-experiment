import { auth, permissions } from '../../services/auth';

/**
 * Pre-declare initial values of claims.
 * It's useful to reset the state after user logged out.
 */
export const initialClaims = Object.freeze({
  token: '',
  user_id: null,
  first_name: '',
  last_name: '',
  email: '',
  client_id: null,
  role_code: null,
});

export default {
  namespaced: true,

  state() {
    return {
      // Form token id claims
      claims: JSON.parse(JSON.stringify(initialClaims)),
      permissions: [],
    };
  },

  getters: {
    isAuth: state => !!state.claims.user_id,
    profile: state => ({
      client_id: state.claims.client_id,
      email: state.claims.email,
      user_id: state.claims.user_id,
      role_code: state.claims.role_code,
      first_name: state.claims.first_name,
      last_name: state.claims.last_name,
      fullName: `${state.claims.first_name} ${state.claims.last_name}`,
    }),
  },

  mutations: {
    setClaims(state, payload) {
      state.claims = {
        ...state.claims,
        ...payload,
      };
    },
    resetClaims(state) {
      state.claims = JSON.parse(JSON.stringify(initialClaims));
    },
  },

  actions: {
    async login({ dispatch }, payload) {
      try {
        const claims = await auth(payload.email, payload.password);
        await dispatch('setAuthState', claims);
      } catch (err) {
        console.error(err);
      }
    },
    logout({ commit }) {
      localStorage.removeItem(process.env.VUE_APP_IDENTITY_KEY);
      commit('resetClaims');
      // Force refresh and return to legacy app
      window.location.href = '/';
    },
    /**
     * Set localStorage and update Vuex
     */
    async setAuthState({ commit }, claims) {
      commit('setClaims', claims);
      localStorage.setItem(process.env.VUE_APP_IDENTITY_KEY, JSON.stringify(claims));
    },
    /**
     * Read z.admin.identity from localStorage, remove it when invalid JSON.
     */
    async loadAuthState({ commit }) {
      try {
        commit('setClaims', {
          ...JSON.parse(localStorage.getItem(process.env.VUE_APP_IDENTITY_KEY)),
        });
      } catch (err) {
        // Incase the localstorage is poluted, we will reset it
        localStorage.removeItem(process.env.VUE_APP_IDENTITY_KEY);
        commit('resetClaims');
      }
    },
    async loadPermissions({ commit }) {
      const { products } = await permissions();
      commit('setPermissions', products);
    },
  },
};
