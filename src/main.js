import Vue from 'vue';
import Element from 'element-ui';

import App from './App.vue';
import router from './router';
import store from './stores';
import './styles/theme.scss';

Vue.config.productionTip = false;

Vue.use(Element);

/**
 * Global properties prefix with 'z_'
 */
Vue.mixin({
  computed: {
    z_user() {
      return store.getters['me/profile'];
    },
    z_isAuth() {
      return store.getters['me/isAuth'];
    },
  },
});

/**
 * Load the auth state before bootstrap the app
 */
store.dispatch('me/loadAuthState')
  .then(() => {
    new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app');
  })
  .catch(() => {
    window.location.href = '/';
  });
