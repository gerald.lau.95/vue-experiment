import Vue from 'vue';
import Router from 'vue-router';

import store from './stores';
import Login from './views/Login.vue';
import Main from './views/Main.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/auth/login',
      name: 'Login',
      component: Login,
      meta: {
        access: 'ALL',
        // in development we just set it to ALL
        // access: 'UNAUTH',
      },
    },
    {
      path: '/',
      name: 'Main',
      component: Main,
      meta: {
        access: 'ALL',
        // in development we just set it to ALL
        // access: 'AUTH',
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  const { meta } = to;
  const isAuth = store.getters['me/isAuth'];
  /**
   * Define what authtication state can access this page
   * access: ALL | UNAUTH | AUTH
   * ALL: regardless of Auth state the page can be access anytimes
   * UNAUTH: The pages can only be access before user login
   * AUTH: The pages can only be access after user logged in
   */
  switch (meta.access) {
    case 'UNAUTH':
      if (isAuth) {
        return next('/');
      }
      break;
    case 'AUTH':
      if (!isAuth) {
        next('/auth/login');
      }
      break;
    default:
      return next();
  }
  return next();
});

export default router;
